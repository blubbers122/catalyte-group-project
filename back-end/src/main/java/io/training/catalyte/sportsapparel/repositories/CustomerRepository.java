package io.training.catalyte.sportsapparel.repositories;

import io.training.catalyte.sportsapparel.entities.Customer;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

  List<Customer> findByEmail(String email);

  boolean existsByEmail(String email);
}


