package io.training.catalyte.sportsapparel.constants;

public class StringConstants {

  public static final String REQUIRED_FIELD = " is a required field";
  public static final String INVALID_ZIP = "Invalid Zip Code. Zip should be a 5 or 9 digit zip code.";
  public static final String INVALID_PHONE_NUMBER = "Invalid phone number. Phone number should meet one of these formats: (111) 222-3333 | 1112223333 | 111-222-3333";
  public static final String INVALID_EMAIL = "Invalid email. Email should meet this format: joelyesupriya@gmail.com";
  public static final String DEFAULT_HEROKU_URL =
      "http://v2-product-search-service.herokuapp.com/v1/products";
  public static final String PAGE_SIZE = "&pageSize=50";
  public static final String MAX_PAGE_SIZE = "&pageSize=5000";
  public static final String PAGE_NUMBER = "&pageNumber=0";
  public static final String QUESTION_MARK = "?";
  public static final String DEMOGRAPHIC_KEY = "demographic=";
  public static final String CATEGORY_KEY = "category=";
  public static final String TYPE_KEY = "type=";
  public static final String AMPERSAND = "&";
  public static final String[] DEMOGRAPHICS = {"Men", "Women", "Kids"};
  public static final String[] CATEGORIES = {"Baseball", "Basketball", "Boxing", "Football", "Golf",
      "Hockey", "Running", "Skateboarding", "Soccer", "Weightlifting"};
  public static final String[] TYPES = {"Belt", "Elbow Pad", "Flip Flop", "Glove", "Hat",
      "Headband", "Helmet", "Hoodie", "Jacket", "Pant", "Pool Noodle", "Shin Guard", "Shoe",
      "Short", "Sock", "Sunglasses", "Tank Top", "Visor", "Wristband"};
}