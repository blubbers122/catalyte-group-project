package io.training.catalyte.sportsapparel.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class SiteSearchTest {

  ResultMatcher okStatus = MockMvcResultMatchers.status().isOk();
  ResultMatcher badData = MockMvcResultMatchers.status().isBadRequest();
  ResultMatcher notFoundStatus = MockMvcResultMatchers.status().isNotFound();
  @Autowired
  private WebApplicationContext wac;
  private MockMvc mockMvc;

  @Before
  public void setUp() throws Exception {
    DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac);
    this.mockMvc = builder.build();
  }

  @Test
  public void testBasicDemographicSearch() throws Exception {
    mockMvc
        .perform(get("/products/search-detailed/Men"))
        .andDo(print())
        .andExpect(okStatus);
  }

  //handle entering nothing and fewer than 3 letters into the search bar on the front end

  @Test
  public void testSomethingOtherThanTypeDemographicOrCategoryInSearch() throws Exception {
    mockMvc
        .perform(get("/products/search-detailed/Comfortable"))
        .andDo(print())
        .andExpect(okStatus);
  }
}
