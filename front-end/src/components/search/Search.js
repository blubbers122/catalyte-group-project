import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import ReactTooltip from 'react-tooltip';
import styles from './Search.module.css';

const Search = () => {
  const [searchQuery, setSearchQuery] = useState('');

  return (
    <form className="input-group">
      <input
        type="text"
        className="form-control"
        aria-label="Search"
        placeholder="Enter a query"
        name="Search"
        onChange={(event) => setSearchQuery(event.target.value)}
      />
      <div className="input-group-append">
        { (searchQuery.length < 3)
          ? (
            <>
              <button
                type="submit"
                data-tip
                data-for="search-button"
                className={`btn ${styles.btn}`}
              >
                <i className="fas fa-search" />
                <ReactTooltip id="search-button" place="right" effect="solid">
                  Please enter at least three characters.
                </ReactTooltip>
              </button>
            </>
          )
          : (
            <Link to={`/search-results/${searchQuery}`}>
              <button
                type="submit"
                data-tip
                data-for="search-button"
                className={`btn ${styles.btn}`}
              >
                <i className="fas fa-search" />
              </button>
            </Link>
          )}
      </div>
    </form>
  );
};

export default Search;
