import React, { useCallback, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { NO_SEARCH_RESULTS, QUASAR_API } from '../Constants';
import ErrorPage from '../error page/ErrorPage';
import styles from './Search.module.css';
import ProductCard from '../product card/ProductCard';

const SearchResults = () => {
  const [searchResults, setSearchResults] = useState([]);
  const [errorCode, setErrorCode] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const { searchQuery } = useParams();

  const getSearchResults = async () => {
    setErrorCode();
    setIsLoading(true);
    let serverError;
    await fetch(`${QUASAR_API}/products/search-detailed/${searchQuery}`, {
      method: 'GET',
      headers: new Headers()
    }).then(async (response) => {
      if (response.ok) {
        setSearchResults(await response.json());
      } else {
        serverError = response.status;
      }
      setIsLoading(false);
    }).catch(() => {
      setErrorCode(serverError || 503);
    });
    setIsLoading(false);
  };
  const getSearchResultsCallBack = useCallback(getSearchResults, [searchQuery]);
  useEffect(getSearchResultsCallBack, [getSearchResultsCallBack]);

  let pageContent;
  if (isLoading) {
    pageContent = (
      <div className={styles.loading}>
        <p className="text-warning">One moment...</p>
      </div>
    );
  } else if (searchResults.length > 0) {
    pageContent = (
      <div className="row row-cols-1 row-cols-md-2 row-cols-lg-3 m-4">
        {searchResults.map((product) => <ProductCard key={product.id} product={product} />)}
      </div>
    );
  } else {
    pageContent = <h3>{NO_SEARCH_RESULTS}</h3>;
  }

  return (
    <div>
      <h1 className={styles.title}>Search Results</h1>
      {errorCode
        ? <ErrorPage errorCode={errorCode} />
        : pageContent}
    </div>
  );
};

export default SearchResults;
